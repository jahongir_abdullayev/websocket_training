
const initialState = {
  message: "",
  progress: 0,
  notification: "",
  connection: false,
};

export default function basicReducer(state = initialState, action) {
  switch (action.type) {
    case "MESSAGE":
      return { ...state, message: action.payload.message };
    case "PROGRESS":
      return { ...state, progress: action.payload.progress };
    case "NOTIFICATION":
      return { ...state, notification: action.payload.notification };
    case "CONNECTION":
      return { ...state, connection: action.payload.connection };
    default:
      return state;
  }
}
