export function updateMessage(message) {
    return {
      type: 'MESSAGE',
      payload: { message },
    }
  }
  
  export function updateProgress(progress) {
    return {
      type: 'PROGRESS',
      payload: { progress },
    }
  }
  export function updateNotification(notification) {
    return {
      type: 'NOTIFICATION',
      payload: { notification },
    }
  }
  
  export function connection(connection) {
    return {
      type: 'CONNECTION',
      payload: { connection },
    }
  }