import { shallowEqual, useSelector } from "react-redux";

export default function Content() {
  const message = useSelector(state => state.basic.messages,shallowEqual)
  return (
    <div className="contnent">
      <div> {message.length ? message : ''} </div>
    </div>
  );
}
