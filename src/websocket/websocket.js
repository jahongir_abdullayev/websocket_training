import { store } from "../redux/store";

export const connectSocket = () => {
  let websocket = new WebSocket("wss://ws.car24.uz/ws-random-2");
  websocket.onopen = () => {
    store.dispatch({
        type:"CONNECTION",
        payload: true
    })
  }
  websocket.onmessage = function (data) {
    const json = JSON.parse(data.data);
    try {
      if (json.type === 'progress') {
          store.dispatch({
              type:"PROGRESS",
              payload: json.data.count
          })
      } else if(json.type === "notification") {
          store.dispatch({
              type: "NOTIFICATION",
              payload : json.data.description
          })
      } else if (json.type === 'message'){
          store.dispatch({
              type: "ADD",
              payload: json.data.description
          })
      }
    } catch (err) {
      console.log(err);
    }
  };

  websocket.onclose = () => {
    console.log('Close')

    store.dispatch({
        type:"CONNECTION",
        payload: false
    })
  }

  websocket.onerror = function(err) {
    console.error('ERROR ', err)
  };
};