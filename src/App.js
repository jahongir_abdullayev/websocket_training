import { notification, Progress } from "antd";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import "./App.css";
import SiderDemo from "./pages/main";
import { connectSocket } from "./websocket/websocket";

function App() {
  const notificate = useSelector((state) => state.basic.notification);
  const connection = useSelector((state) => state.basic.connection);
  const progress = useSelector((state) => state.basic.progress)

  console.log(connection);

  const [error, setError] = useState(false);

  useEffect(() => {
    connectSocket();
  }, []);

  useEffect(() => {
    if (typeof connection === "function") {
      setError(true);
    } else {
      setError(false);
    }
  }, [connection]);


  return (
    <div className="App">
      <SiderDemo/>
    </div>
  );
}

export default App;
