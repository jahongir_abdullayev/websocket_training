# Task

1. Connect to wss://ws.car24.uz/ws-random-2

2. You can get three type of message

    - progress

    - notification

    - message

3. Show the progress on the top left corner of the page

4. Show the notifications on the top right corner of the page

5. Show the messages on the **Content**

5. Strong Junior, Middle level: Add reload actionBar on the top of the webpage header.
    - It should be able to reconnect when you click
    - It should be red background if disconnected and green ( with smooth disapperance after 1 second ) when it is connected.
    - Use it if your are currently working with EDA project. If not, Create new project and add this functionality.
![image.png](./image.png)
